# cflite-example

Example for using [ClusterFuzzLite](https://github.com/google/clusterfuzzlite) with GitLab.

## Description

ClusterFuzzLite (CFL) added support for GitLab just recently.
This repository aims to show how fuzzing with ClusterFuzzLite
can be done in GitLab.

It is based on the following documentation:

* https://google.github.io/clusterfuzzlite/build-integration/
* https://google.github.io/clusterfuzzlite/running-clusterfuzzlite/gitlab/

## Coverage Report

See the [coverage report](https://securitykernel.gitlab.io/cflite-example/cflite-coverage/coverage-latest/report/linux/report.html)
from the main branch.
