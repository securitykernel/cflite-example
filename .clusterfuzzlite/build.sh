#!/bin/bash -eu

# build project
# e.g.
# ./autogen.sh
# ./configure
# make -j$(nproc) all

# build fuzzers
# e.g.
# $CXX $CXXFLAGS -std=c++11 -Iinclude \
#     /path/to/name_of_fuzzer.cc -o $OUT/name_of_fuzzer \
#     $LIB_FUZZING_ENGINE /path/to/library.a

#$CXX $CXXFLAGS -std=c++11 \
#    test_fuzzer.cc -o $OUT/test_fuzzer \
#    $LIB_FUZZING_ENGINE

cmake -DBUILD_SHARED_LIBS=OFF     -DCMAKE_RUNTIME_OUTPUT_DIRECTORY=$OUT \
    -DCMAKE_EXE_LINKER_FLAGS=$LIB_FUZZING_ENGINE .
make -j$(nproc) VERBOSE=1

# Optional: Copy dictionaries and options files.
# cp $SRC/*.dict $SRC/*.options $OUT/
